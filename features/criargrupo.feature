# encoding: utf-8
# language: pt


Funcionalidade: Criar Grupo
	Sendo usuário do Whatsapp 
	Posso criar grupos de conversas
	Para que possa conversar com varias pessoas ao mesmo tempo
	
	Cenário: Criando um Grupo
		Dado que eu esteja autenticado no aplicativo
			E esteja na aba conversas
		Quando clicar em "Novo Grupo"
			E selecionar um ou mais contatos
			E clicar em "Seguinte"
			E informar "<nome do grupo>" como valor de "Nome do Grupo"
			E clicar em "Criar"
		Então uma janela de conversa entre mim e os contatos selecionados deve ser aberta
		
	Cenário: Adicionando contatos ao Grupo
		Dado que eu esteja com a janela do conversa do grupo aberta
			E esteja com status de adiministrador do grupo
		Quando clicar no nome do grupo
			E clicar em "Adicionar Participantes"
			E selecionar um ou mais contatos
			E clicar em "Adic."
			E confirmar "Adic."
		Então os contatos selecionados devem ser adicionados ao grupo
			E aparecer na lista de participantes
			
