# encoding: utf-8
# language: pt


Funcionalidade: Silenciar conversa
	Sendo usuário do Whatsapp
	Posso sileinciar as conversas
	Para que não apareça notificações da conversa
	
	Cenário: Silenciando conversa
		Dado que esteja autenticado no aplicativo
			E esteja na aba conversa
		Quando deslizar o dedo para esquerda em cima da conversa
			E clicar em "... Mais"
			E clicar em "Silenciar"
			E escolher o tempo de silêncio
		Então a conversa não devera apresentar notificações no periodo selecionado